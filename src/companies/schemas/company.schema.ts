import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type CompanyDocument = HydratedDocument<Company>;

@Schema({ timestamps: true })
export class Company {
    @Prop()
    name: string;

    @Prop()
    address: string;

    @Prop()
    description: string;

    @Prop()
    logo: string;

    @Prop({ type: Object, required: false })
    CreatedBy: {
        _id: string,
        email: string
    }
    @Prop({ type: Object, required: false })
    UpdatedBy: {
        _id: string,
        email: string
    }
    @Prop({ type: Object, required: false })
    DeletedBy: {
        _id: string,
        email: string
    }
    @Prop()
    CreatedAt: Date;

    @Prop()
    UpdatedAt: Date;

    @Prop()
    isDeleted: Boolean;

    @Prop()
    DeletedAt: Date;
}

export const CompanySchema = SchemaFactory.createForClass(Company);