import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateUserDto, RegisterUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './schemas/user.schema';
import mongoose, { Model } from 'mongoose';
import { genSaltSync, hashSync, compareSync } from "bcryptjs"
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import { log } from 'console';
import { IUser } from './users.interface';
import aqp from 'api-query-params';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: SoftDeleteModel<UserDocument>) { }

  getHashPassword = (plainPassword: string) => {
    let salt = genSaltSync(10);
    let hash = hashSync(plainPassword, salt);
    return hash
  }

  async registerUser(user: RegisterUserDto) {
    const { name, email, password, age, gender, address, refreshToken } = user
    const hashPassword = this.getHashPassword(password)

    //check exist email
    const isExistEmail = await this.userModel.findOne({ email })
    if (isExistEmail) {
      throw new BadRequestException(`The email: ${email} has been existed`)
    }

    let newRegisterUser = await this.userModel.create({
      name, email, password: hashPassword, age, gender, address, refreshToken, role: "USER"
    })
    return newRegisterUser
  }

  async create(createUserDto: CreateUserDto, user: IUser) {
    const { name, email, password, age, gender, address, role, company } = createUserDto
    let isExistEmail = await this.userModel.findOne({ email })
    if (isExistEmail) {
      throw new BadRequestException(`This email: ${email} has been existed`)
    }

    const hashPassword = this.getHashPassword(password)

    let newUser = await this.userModel.create({
      email: email,
      password: hashPassword,
      name: name,
      age: age,
      gender: gender,
      address: address,
      role: role,
      company: company,
      CreatedBy: {
        _id: user?._id,
        name: user?.name
      }
    })
    return newUser;
  }

  async findAll(currentPage: number, limit: number, query: string) {
    const { filter, sort, population } = aqp(query);
    delete filter.current;
    delete filter.pageSize;

    let offset = (+currentPage - 1) * (+limit);
    let defaultLimit = +limit ? +limit : 10;

    const totalItems = (await this.userModel.find(filter)).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const result = await this.userModel.find(filter)
      .skip(offset)
      .limit(defaultLimit)
      // @ts-ignore: Unreachable code error
      .sort(sort)
      .populate(population)
      .exec();
    return {
      meta: {
        current: currentPage, //trang hiện tại
        pageSize: limit, //số lượng bản ghi đã lấy
        pages: totalPages, //tổng số trang với điều kiện query
        total: totalItems // tổng số phần tử (số bản ghi)
      },
      result //kết quả query
    }

  }

  async findOne(id: string) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return `not found user`
    }
    return await this.userModel.findOne({ _id: id })
      .select("-password")
      .populate({ path: "role", select: { name: 1, _id: 1 } })
  }

  findOneUserByUsername(username: string) {

    return this.userModel.findOne({
      email: username
    }).populate({ path: "role", select: { name: 1, permission: 1 } })
  }
  isValidPassword(password: string, hashPassword: string) {
    return compareSync(password, hashPassword);
  }

  async update(updateUserDto: UpdateUserDto, user: IUser) {
    const updated = await this.userModel.updateOne({
      _id: updateUserDto._id
    }, {
      ...updateUserDto, updatedBy: {
        _id: user._id,
        email: user.email
      }
    },);
    return updated
  }

  async remove(id: string, user: IUser) {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return `not found user`
    }
    const foundUser = await this.userModel.findById(id)
    if (foundUser.email === "admin@gmail.com") {
      throw new BadRequestException("Can't delete this user")
    }
    await this.userModel.updateOne(
      { _id: id },
      {
        deletedBy: {
          _id: user._id,
          email: user.email
        }
      })
    return this.userModel.softDelete({
      _id: id
    })
  }

  updateUserRefreshToken = async (refresh_Token: string, _id: string) => {
    let result = await this.userModel.updateOne({ _id }, { refreshToken: refresh_Token })
    return result
  }

  findUserByToken = async (refreshToken: string) => {
    return await this.userModel.findOne(
      { refreshToken })
  }

  logOutUser = async (refresh_Token: string, _id: string) => {
    await this.userModel.updateOne({ _id }, { refresh_Token })
  }
}
