import { BadRequestException, Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { IUser } from 'src/users/users.interface';
import { RegisterUserDto } from 'src/users/dto/create-user.dto';
import { ConfigService } from '@nestjs/config';
import ms from 'ms';
import { Response } from 'express';


@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private jwtService: JwtService,
        private configService: ConfigService) { }

    async validateUser(username: string, pass: string): Promise<any> {
        const user = await this.usersService.findOneUserByUsername(username);
        if (user) {
            const isValidPassword = this.usersService.isValidPassword(pass, user.password)
            if (isValidPassword) {
                return user
            }
        }
        return null;
    }

    async login(user: IUser, response: Response) {
        const { _id, name, email, role } = user

        const payload = {
            sub: "token login",
            iss: "from server",
            _id,
            name,
            email,
            role,
        };
        const refresh_Token = this.createRefreshToken(payload)

        //update refresh token in user database
        await this.usersService.updateUserRefreshToken(refresh_Token, _id)

        //set refresh_Token as cookies
        response.cookie('refresh_token', refresh_Token, {
            httpOnly: true,
            maxAge: ms(this.configService.get<string>("JWT_REFRESH_TOKEN_EXPIRE"))
        })

        return {
            access_token: this.jwtService.sign(payload),
            user: {
                _id,
                name,
                email,
                role,
            }
        };
    }

    async registerNewUser(user: RegisterUserDto) {
        let newUser = await this.usersService.registerUser(user)
        return {
            _id: newUser?._id,
            createdAt: newUser?.createdAt
        }
    }

    createRefreshToken(payload) {
        const refresh_Token = this.jwtService.sign(payload, {
            secret: this.configService.get<string>("JWT_REFRESH_TOKEN_SECRET"),
            expiresIn: ms(this.configService.get<string>("JWT_REFRESH_TOKEN_EXPIRE"))
        })
        return refresh_Token
    }

    processNewToken = async (refreshToken: string, response: Response) => {
        try {
            this.jwtService.verify(refreshToken, {
                secret: this.configService.get<string>("JWT_REFRESH_TOKEN_SECRET"),
            })

            let user = await this.usersService.findUserByToken(refreshToken)
            // @ts-ignore
            if (user) {
                const { _id, name, email, role } = user

                const payload = {
                    sub: "token login",
                    iss: "from server",
                    _id,
                    name,
                    email,
                    role,
                };
                const refresh_Token = this.createRefreshToken(payload)

                //update refresh token in user database
                await this.usersService.updateUserRefreshToken(refresh_Token, _id.toString())

                //set refresh_Token as cookies
                response.clearCookie("refresh_token")
                response.cookie('refresh_token', refresh_Token, {
                    httpOnly: true,
                    maxAge: ms(this.configService.get<string>("JWT_REFRESH_TOKEN_EXPIRE"))
                })

                return {
                    access_token: this.jwtService.sign(payload),
                    user: {
                        _id,
                        name,
                        email,
                        role,
                    }
                };

            } else {
                throw new BadRequestException("Refresh token is invalid. Please login")
            }


        } catch (error) {
            throw new BadRequestException("Refresh token is invalid. Please login")
        }
    }

    logOutUser = async (user, response) => {
        let res = await this.usersService.logOutUser("", user._id)
        response.clearCookie("refresh_token");
        return "ok"
    }

}
