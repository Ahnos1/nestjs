import { Body, Controller, Get, Post, Req, Res, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Public, ResponseMessage, User } from 'src/decorator/customize';
import { LocalAuthGuard } from './local-auth.guard';
import { CreateUserDto, RegisterUserDto } from 'src/users/dto/create-user.dto';
import { Request, Response } from 'express';
import { IUser } from 'src/users/users.interface';

@Controller("auth")
export class AuthController {
    constructor(private readonly authService: AuthService) { }

    @Public()
    @UseGuards(LocalAuthGuard)
    @ResponseMessage("User Login")
    @Post("/login")
    handleLogin(@Req() req, @Res({ passthrough: true }) response: Response) {
        return this.authService.login(req.user, response);
    }
    @Public()
    @ResponseMessage("Register a new user")
    @Post("/register")
    registerNewUser(@Body() registerUserDto: RegisterUserDto) {
        return this.authService.registerNewUser(registerUserDto)
    }

    @ResponseMessage("Check JWT token when refresh page")
    @Get("/account")
    getAccount(@User() user: IUser) {
        return { user }
    }

    @Public()
    @ResponseMessage("Get User by refresh token")
    @Get("/refresh")
    handleRefreshToken(@Req() request: Request, @Res({ passthrough: true }) response: Response) {
        const refreshToken = request.cookies[ "refresh_token" ]
        return this.authService.processNewToken(refreshToken, response)
    }


    @ResponseMessage("Logout User")
    @Post("/logout")
    logOutUser(@User() user: IUser, @Res({ passthrough: true }) response: Response) {
        return this.authService.logOutUser(user, response)
    }
}

