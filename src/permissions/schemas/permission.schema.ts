import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument, Mongoose } from 'mongoose';

export type PermissionDocument = HydratedDocument<Permission>;

@Schema({ timestamps: true })
export class Permission {
    @Prop()
    name: string;

    @Prop()
    apiPath: string

    @Prop()
    method: string;

    @Prop()
    module: string;

    @Prop({ type: Object })
    createdBy: {
        _id: mongoose.Schema.Types.ObjectId
        name: string
    };

    @Prop({ type: Object, required: false })
    updatedBy: {
        _id: string,
        email: string
    }

    @Prop({ type: Object, required: false })
    deletedBy: {
        _id: string,
        email: string
    }

    @Prop()
    createdAt: Date;

    @Prop()
    updatedAt: Date;

    @Prop()
    isDeleted: Boolean;

    @Prop()
    deletedAt: Date;

}

export const PermissionSchema = SchemaFactory.createForClass(Permission);