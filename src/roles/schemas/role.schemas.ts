import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { HydratedDocument, Mongoose } from 'mongoose';
import { Permission } from 'src/permissions/schemas/permission.schema';

export type RoleDocument = HydratedDocument<Role>;

@Schema({ timestamps: true })
export class Role {
    @Prop()
    name: string;

    @Prop()
    description: string

    @Prop()
    isActive: boolean;

    @Prop({ type: [ mongoose.Schema.Types.ObjectId ], ref: Permission.name })
    permission: Permission[]

    @Prop({ type: Object })
    createdBy: {
        _id: mongoose.Schema.Types.ObjectId
        name: string
    };

    @Prop({ type: Object, required: false })
    updatedBy: {
        _id: string,
        email: string
    }

    @Prop({ type: Object, required: false })
    deletedBy: {
        _id: string,
        email: string
    }

    @Prop()
    createdAt: Date;

    @Prop()
    updatedAt: Date;

    @Prop()
    isDeleted: Boolean;

    @Prop()
    deletedAt: Date;

}

export const RoleSchema = SchemaFactory.createForClass(Role);