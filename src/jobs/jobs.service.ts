import { Injectable } from '@nestjs/common';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { IUser } from 'src/users/users.interface';
import { InjectModel } from '@nestjs/mongoose';
import { Job, JobDocument } from './schemas/job.schema';
import { SoftDeleteModel } from 'soft-delete-plugin-mongoose';
import aqp from 'api-query-params';

@Injectable()
export class JobsService {
  constructor(@InjectModel(Job.name) private JobModel: SoftDeleteModel<JobDocument>) { }

  async create(createJobDto: CreateJobDto, user: IUser) {
    return await this.JobModel.create({
      ...createJobDto,
      createdBy: {
        _id: user._id,
        name: user.name
      }
    })
  }

  async fetchJobWithPaginate(currentPage: number, limit: number, query: string) {
    const { filter, sort, population } = aqp(query);
    delete filter.current;
    delete filter.pageSize;

    let offset = (+currentPage - 1) * (+limit);
    let defaultLimit = +limit ? +limit : 10;

    const totalItems = (await this.JobModel.find(filter)).length;
    const totalPages = Math.ceil(totalItems / defaultLimit);

    const result = await this.JobModel.find(filter)
      .skip(offset)
      .limit(defaultLimit)
      // @ts-ignore: Unreachable code error
      .sort(sort)
      .populate(population)
      .exec();
    return {
      meta: {
        current: currentPage, //trang hiện tại
        pageSize: limit, //số lượng bản ghi đã lấy
        pages: totalPages, //tổng số trang với điều kiện query
        total: totalItems // tổng số phần tử (số bản ghi)
      },
      result //kết quả query
    }
  }

  async findOne(id: string) {
    return await this.JobModel.findOne({ _id: id })
  }

  async update(id: string, updateJobDto: UpdateJobDto, user: IUser) {
    return await this.JobModel.updateOne({ _id: id }, {
      ...updateJobDto,
      updatedBy: {
        name: updateJobDto.name,
        _id: user._id
      }
    })
  }

  async remove(id: string, user: IUser) {
    await this.JobModel.updateOne({ _id: id }, {
      deletedBy: {
        _id: user._id,
        name: user.name
      }
    })
    return await this.JobModel.softDelete({ _id: id })
  }
}
