import { Type } from "class-transformer";
import { IsArray, IsNotEmpty, IsObject, IsString, Validate, ValidateNested } from "class-validator";
import mongoose from "mongoose";

export class Company {
    @IsNotEmpty()
    _id: mongoose.Schema.Types.ObjectId

    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    logo: string
}

export class CreateJobDto {
    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    @IsArray()
    @IsString({ each: true })
    skills: string[];

    @IsNotEmpty()
    @IsObject()
    @ValidateNested()
    @Type(() => Company)
    company: Company

    location: string;


    @IsNotEmpty()
    salary: number;

    @IsNotEmpty()
    quantity: number;

    @IsNotEmpty()
    level: string

    @IsNotEmpty()
    description: string;
}
